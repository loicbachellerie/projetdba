package ca.qc.cvm.dba.verifacme.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import com.objectif8.libraries.common.network.ClientAdapter;
import com.objectif8.libraries.common.network.NetworkAdapter;

import ca.qc.cvm.dba.verifacme.dao.DBConnection;
import ca.qc.cvm.dba.verifacme.dao.TestDAO;
import ca.qc.cvm.dba.verifacme.event.StartTestEvent;
import ca.qc.cvm.dba.verifacme.event.CommonEvent;
import ca.qc.cvm.dba.verifacme.event.UIEvent;
import ca.qc.cvm.dba.verifacme.event.UIEvent.TextType;

public class MngApplication extends Observable implements Runnable {
    private List<CommonEvent> eventQueue;
    
    public MngApplication() {
    	eventQueue = new ArrayList<CommonEvent>();

		
		//if (success) {
			// Test de connexion au programme ma�tre
			NetworkAdapter client = new ClientAdapter("10.57.47.100", 5554, "1.0.0");
			client.connect();
		//}
    }
    
    public void addEvent(CommonEvent event) {
    	synchronized(MngApplication.class) {
    		eventQueue.add(event);
    	}
    }

	public void run() {
        synchronized (MngApplication.class) {
            CommonEvent event = eventQueue.remove(0);
            processEvent(event);
        }
    }
	
	private void processEvent(CommonEvent event) {
		if (event.getType() == CommonEvent.Type.BUTTON_EVENT) {		
			try {
				setChanged();
				notifyObservers(new UIEvent(UIEvent.UIType.ShowMessage, "D�marrage des tests... veuillez patienter.", TextType.Normal, false));
				
				// Can connect with ACME_USR
				prepareTest("Test de connection avec ACME_USR...");
				boolean success = TestDAO.connect();
				logResult(success);
				
				if (success) {
					// Cannot have more than 2 active sessions
					
					prepareTest("Test permettant de v�rifier le nombre maximale de connexions (2)...");
					success = TestDAO.test2ConnectionsMax();
					logResult(success);
				}
				
				
				if (success) {
					// Cannot connect with ACME_CREATEUR
					
					prepareTest("Le compte ACME_CREATEUR devrait �tre d�sactiv�...");
					success = TestDAO.testCreator();
					logResult(success);
				}
				
				if (success) {
					// Test synonyms : PRODUITS, CLIENTS, BACKUPLOGS, CATEGORIES
					
					prepareTest("Les tables et leur synonyme existent. Le compte ACME_USR peut interroger les tables...");
					success = TestDAO.testTables();
					logResult(success);
				}
				
				if (success) {
					// Test table owner is ACME_CREATEUR
					
					prepareTest("V�rification que les tables appartiennent � ACME_CREATEUR...");
					success = TestDAO.testTableOwnership();
					logResult(success);
				}
				
				if (success) {
					setChanged();
					notifyObservers(new UIEvent(UIEvent.UIType.ShowMessage, "\n\n!!! FIN DES TESTS !!!", TextType.Success, true));
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void prepareTest(String text) {
		setChanged();
		notifyObservers(new UIEvent(UIEvent.UIType.ShowMessage, text, TextType.Normal, false));
		try {
			Thread.sleep(1000);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void logResult(boolean success) {
		setChanged();
		
		if (success) {
			notifyObservers(new UIEvent(UIEvent.UIType.ShowMessage, "- OK", TextType.Success, false));
		}
		else {
			notifyObservers(new UIEvent(UIEvent.UIType.ShowMessage, "- Error... STOPPING tests", TextType.Error, true));
		}
		
		try {
			Thread.sleep(500);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	    	
	public void exit() {
		DBConnection.closeConnection();
		System.exit(0);
	}
}
