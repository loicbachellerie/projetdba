package ca.qc.cvm.dba.verifacme.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class TestDAO {

	public static boolean connect() {
		Connection connection = DBConnection.getConnection();
		boolean success = connection != null;
		
		return success;
	}
	
	public static boolean test2ConnectionsMax() {
		Connection connection = DBConnection.getConnection();
		
		Connection connection2 = DBConnection.newConnection();
		Connection connection3 = DBConnection.newConnection();
		
		boolean success = false;
		
		if (connection != null && connection2 != null && connection3 == null) {
			success = true;
		}
		
		if (connection2 != null) {
			try {
				connection2.close();
			} 
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		if (connection3 != null) {
			try {
				connection3.close();
			} 
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return success;
	}
	
	public static boolean testCreator() {
		Connection connection = DBConnection.getCreatorConnection();
		boolean success = connection == null;
		
		if (connection != null) {
			try {
				connection.close();
			} 
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return success;
	}
	
	public static boolean testTables() {
		Connection connection = DBConnection.getConnection();
		boolean success = false;
		
		Statement stmt = null;
	    try {
	        stmt = connection.createStatement();
	        ResultSet rs = stmt.executeQuery("SELECT * FROM PRODUITS");
	        
	        rs = stmt.executeQuery("SELECT * FROM CATEGORIES");
	        
	        rs = stmt.executeQuery("SELECT * FROM BACKUPLOGS");
	        
	        rs = stmt.executeQuery("SELECT * FROM CLIENTS");
	        
	        success = true;
	    } 
	    catch (Exception e ) {
	    	e.printStackTrace();
	    } 
	    finally {
	        if (stmt != null) { 
	        	try {
					stmt.close();
				} catch (Exception e) {
					e.printStackTrace();
				} 
	        }
	    }
		
		return success;
	}
	
	public static boolean testTableOwnership() {
		Connection connection = DBConnection.getConnection();
		boolean success = false;
		
		Statement stmt = null;
	    try {
	        stmt = connection.createStatement();
	        ResultSet rs = stmt.executeQuery("SELECT OWNER FROM ALL_TABLES WHERE TABLE_NAME = 'PRODUITS'");
	        if (!rs.next() || !"ACME_CREATEUR".equals(rs.getString("OWNER"))) {
	        	stmt.close();
	        	return false;
	        }	        
	        
	        rs = stmt.executeQuery("SELECT OWNER FROM ALL_TABLES WHERE TABLE_NAME = 'CATEGORIES'");
	        if (!rs.next() || !"ACME_CREATEUR".equals(rs.getString("OWNER"))) {
	        	stmt.close();
	        	System.out.println("2");
	        	return false;
	        }
	        
	        rs = stmt.executeQuery("SELECT OWNER FROM ALL_TABLES WHERE TABLE_NAME = 'BACKUPLOGS'");
	        if (!rs.next() || !"ACME_CREATEUR".equals(rs.getString("OWNER"))) {
	        	stmt.close();
	        	System.out.println("3");
	        	return false;
	        }
	        
	        rs = stmt.executeQuery("SELECT OWNER FROM ALL_TABLES WHERE TABLE_NAME = 'CLIENTS'");
	        if (!rs.next() || !"ACME_CREATEUR".equals(rs.getString("OWNER"))) {
	        	stmt.close();
	        	System.out.println("4");
	        	return false;
	        }
	        
	        success = true;
	    } 
	    catch (Exception e ) {
	    	e.printStackTrace();
	    } 
	    finally {
	        if (stmt != null) { 
	        	try {
					stmt.close();
				} catch (Exception e) {
					e.printStackTrace();
				} 
	        }
	    }
		
		return success;
	}
	
}
