package ca.qc.cvm.dba.verifacme.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	private static Connection connection;
	
	public static Connection getConnection() {
		if (connection == null) {
			connection = newConnection();
		}
		
		return connection;
	}
	
	public static Connection newConnection() {
		Connection connection = null;
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");

			connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "ACME_USR", "AAAaaa333");
		} 
		catch (ClassNotFoundException e) {
			System.out.println("Where is your Oracle JDBC Driver?");
			e.printStackTrace();
		}
		catch (SQLException e) {
			if (!e.getMessage().contains("ORA-02391:")) {
				System.out.println("Connection Failed! Check output console");
				e.printStackTrace();
			}
		}
		
		return connection;
	}
	
	public static Connection getCreatorConnection() {
		Connection connection = null;
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");

			connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "ACME_CREATEUR", "AAAaaa222");
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		catch (SQLException e) {
			if (!e.getMessage().contains("ORA-02391:") &&
				!e.getMessage().contains("CREATE SESSION") &&
				!e.getMessage().contains("ORA-28000")) {
				e.printStackTrace();
			}
		}
		
		return connection;
	}
	
	public static void closeConnection() {
		if (connection != null) {
			try {
				connection.close();
			} 
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
