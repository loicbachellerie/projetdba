package ca.qc.cvm.dba.verifacme;

import ca.qc.cvm.dba.verifacme.app.Facade;
import ca.qc.cvm.dba.verifacme.view.FrameMain;

public class Main {

	public static void main(String[] args) {
		
		try {
			FrameMain frame = new FrameMain();
			frame.setVisible(true);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
				
	}
}
