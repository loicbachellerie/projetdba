package ca.qc.cvm.dba.verifacme.event;

public class UIEvent extends CommonEvent {
	public enum UIType {ShowMessage}
	public enum TextType { Normal, Success, Error };
	
	private UIType uiType;
	private String text;
	private TextType textType;
	private boolean completed;
	
	public UIEvent(UIType type, String text, TextType textType, boolean completed) {
		super(CommonEvent.Type.UI);
		
		uiType = type;
		
		this.text = text;
		this.textType = textType;
		this.completed = completed;
	}
	
	public boolean isCompleted() {
		return completed;
	}
	
	public UIType getUIType() {
		return uiType;
	}
	
	public String getText() {
		return text;
	}
	
	public TextType getTextType() {
		return textType;
	}
}
