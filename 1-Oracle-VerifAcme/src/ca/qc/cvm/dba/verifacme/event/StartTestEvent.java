package ca.qc.cvm.dba.verifacme.event;

public class StartTestEvent extends CommonEvent {
		
	public StartTestEvent() {
		super(CommonEvent.Type.BUTTON_EVENT);
	}
}
