package ca.qc.cvm.dba.doyouknow.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

public class UserDAO {
	public static enum Relation implements RelationshipType { KNOWS, OLDER_THAN }
	private final static String DB_NAME = "DoYouKnowDB";
	private GraphDatabaseService graphDB;

	public UserDAO() {
		boolean mustCreate = isNew();
		
	    graphDB = new GraphDatabaseFactory().newEmbeddedDatabase(new File(DB_NAME));
		
	    Runtime.getRuntime().addShutdownHook( new Thread() {
	        @Override
	        public void run() {
	        	graphDB.shutdown();
	        }
	    } );
	    
 		if (mustCreate) { 
 			try (Transaction tx = graphDB.beginTx()) { 			
	 			Node fred = creerNoeud("Fr�d�ric");
	 			Node eric = creerNoeud("�ric");
	 			Node paul = creerNoeud("Paul");
	 			Node pierrePaul = creerNoeud("Pierre-Paul");
	 			Node jeanChristophe = creerNoeud("Jean-Christophe");
	 			Node marcel = creerNoeud("Marcel");
	 			Node marcAndre = creerNoeud("Marc-Andr�");
	 			Node richard = creerNoeud("Richard");
	 			Node alain = creerNoeud("Alain");
	 			Node michelle = creerNoeud("Michelle");
	 			Node jeanMarc = creerNoeud("Jean-Marc");
	 			Node suzanne = creerNoeud("Suzanne");
	 			
	 			fred.createRelationshipTo(eric, Relation.KNOWS);
	 			eric.createRelationshipTo(fred, Relation.KNOWS);
	 			eric.createRelationshipTo(paul, Relation.KNOWS);
	 			paul.createRelationshipTo(fred, Relation.KNOWS);
	 			paul.createRelationshipTo(marcel, Relation.KNOWS);
	 			eric.createRelationshipTo(jeanChristophe, Relation.KNOWS);
	 			eric.createRelationshipTo(richard, Relation.KNOWS);
	 			jeanMarc.createRelationshipTo(richard, Relation.KNOWS);
	 			jeanMarc.createRelationshipTo(marcel, Relation.KNOWS);
	 			suzanne.createRelationshipTo(marcel, Relation.KNOWS);	 			
	 			michelle.createRelationshipTo(marcel, Relation.KNOWS);
	 			marcAndre.createRelationshipTo(eric, Relation.KNOWS);
	 			marcAndre.createRelationshipTo(paul, Relation.KNOWS);
	 			paul.createRelationshipTo(marcel, Relation.KNOWS);
	 			fred.createRelationshipTo(marcel, Relation.KNOWS);
	 			marcel.createRelationshipTo(eric, Relation.KNOWS);
	 			eric.createRelationshipTo(marcel, Relation.KNOWS);
	 			
	 			fred.createRelationshipTo(marcAndre, Relation.OLDER_THAN);
	 			eric.createRelationshipTo(fred, Relation.OLDER_THAN);
	 			marcel.createRelationshipTo(eric, Relation.OLDER_THAN);
	 			marcel.createRelationshipTo(paul, Relation.OLDER_THAN);
	 			paul.createRelationshipTo(pierrePaul, Relation.OLDER_THAN);
	 			paul.createRelationshipTo(jeanChristophe, Relation.OLDER_THAN);
	 			fred.createRelationshipTo(richard, Relation.OLDER_THAN);
	 			fred.createRelationshipTo(alain, Relation.OLDER_THAN);
	 			alain.createRelationshipTo(michelle, Relation.OLDER_THAN);
	 			alain.createRelationshipTo(jeanMarc, Relation.OLDER_THAN);
	 			alain.createRelationshipTo(suzanne, Relation.OLDER_THAN);
	 			
	 			tx.success();
 			}
 		}
	}
	
	public boolean isNew() {
		return !(new File(DB_NAME).exists());
	}
	
	public Node creerNoeud(String name) {
		Node node = graphDB.createNode();
		node.setProperty( "name", name );
		
		return node;
	}
		
	public boolean ajouterRelation(Node noeud1, Relation relation, Node noeud2) {
		boolean success = false;
			
		try (Transaction tx = graphDB.beginTx()) {
			noeud1.createRelationshipTo( noeud2, relation );
 			tx.success();
			success = true;
		}
		
		return success;
	}
	
	/**
	 * Ex�cute une requ�te Cypher. 
	 * 
	 * @param requete. Le RETURN de la requ�te doit retourner Name. Exemple : ... return n.name
	 */
	private List<String> executerRequeteCypher(String requete) {
		return this.executerRequeteCypher(requete, null);
	}
			
	private List<String> executerRequeteCypher(String requete, String name) {
		List<String> result = new ArrayList<String>();
		Map<String, Object> params = new HashMap<String, Object>();
		
		if (name != null) {
			params.put("p1", name);
		}

		Result executionResult = graphDB.execute( requete, params );
		
		try (Transaction tx = graphDB.beginTx()) {
			while (executionResult.hasNext()) {
				Map<String, Object> row  = executionResult.next();
				
				for ( Map.Entry<String, Object> column : row.entrySet() ) {
					result.add(column.getValue().toString());
			    }
			}
		}
		
		return result;
	}

	/**
	 * Chercher la liste de tous les usagers
	 * 
	 * @return La liste compl�te!
	 */
	public List<String> findAllUsers() {
		List<String> result = executerRequeteCypher("MATCH (a) RETURN a.name");
		return result;
	}
	
	/**
	 * Pour l'usager, retourner toutes les personnes qu'il connait
	 * 
	 * @param user : nom de l'usager
	 * @return La liste de personnes qu'il connait
	 */
	public List<String> getDirectConnectionsOf(String user) {
		
		List<String> result = executerRequeteCypher("MATCH (a)-[:KNOWS]->(b) WHERE a.name = {p1} RETURN b.name ORDER BY b.name", user);
		
		return result;
	}
	
	/**
	 * Retourner la liste des personnes qui sont connues de 2 personnes ou plus
	 * 
	 * @return liste des personnes populaires
	 */
	public List<String> getPopularUsers() {
		List<String> result = executerRequeteCypher("MATCH (a), (b), (c) WHERE (a)-[:KNOWS]->(b) AND (c)-[:KNOWS]->(b) RETURN DISTINCT b.name");
		return result;
	}
	
	/**
	 * Si deux contacts directs d'une personne connaissent la m�me personne,
	 * mais que celui-ci ne la connait pas... pourquoi ne pas lui proposer?
	 * (i.e. Les amis de mes amis sont mes amis)
	 * 
	 * @param nom de la personne
	 * 
	 * @return liste de propositions
	 */
	public List<String> proposeConnection(String user) {
		List<String> result = new ArrayList<String>();
		result = executerRequeteCypher("MATCH (a {name:{p1}})-[:KNOWS]->(b)-[:KNOWS]->(d), (a {name:{p1}})-[:KNOWS]->(c)-[:KNOWS]->(d) WHERE (c)<>(d) RETURN DISTINCT d.name", user);
		return result;
	}
	
	/**
	 * Certaines personnes ne sont connues de personnes... qui?
	 * 
	 * @return les personnes qui sont m�connues
	 */
	public List<String> checkUnconnected() {
		List<String> result = new ArrayList<String>();
		
		return result;
	}
	
	/**
	 * Quelle est la personne la plus �g�e?
	 * 
	 * @return nom
	 */
	public List<String> getOldest() {
		List<String> result = new ArrayList<String>();
		
		return result;
	}
}
