package ca.qc.cvm.dba.memos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ca.qc.cvm.dba.memos.entity.Category;
import ca.qc.cvm.dba.memos.entity.User;
import ca.qc.cvm.dba.memos.util.BCrypt;

public class CategoryDAO {

	/**
	 * M�thode qui permet d'ajouter une cat�gorie. 
	 * Note : Le nom de la cat�gorie doit �tre unique et non vide!
	 * 
	 * @param id L'id de l'usager
	 * @param name Le nom de la cat�gorie 
	 * @return vrai/faux, selon si l'op�ration a fonctionn�e ou pas
	 */
	public static boolean addCategory(int userId, String name) {
		boolean success = false;
		Connection connection = DBConnection.getConnection();
		
		try {
			PreparedStatement statement = connection.prepareStatement("INSERT INTO categories (id_user, nom_categorie) VALUES (?,?)");
			statement.setInt(1, userId);
			statement.setString(2, name);
			statement.execute();
			statement.close();
			success = true;
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return success;
	}
	
	/**
	 * M�thode qui permet de supprimer une cat�gorie et les m�mos associ�s
	 * 
	 * @param id de la cat�gorie
	 * @return vrai/faux, selon si l'op�ration a fonctionn�e ou pas
	 */
	public static boolean deleteCategory(int id) {
		boolean success = false;
		Connection connection = DBConnection.getConnection();
		try {
			PreparedStatement statement = connection.prepareStatement("DELETE FROM categories WHERE id = ?");
			statement.setInt(1, id);
			statement.execute();
			statement.close();
			success = true;
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return success;
	}
	
	/**
	 * M�thode qui retourne l'ensemble des cat�gories
	 * 
	 * @param id L'id de l'usager
	 * @return une liste de cat�gories (si aucune cat�gorie, retourner une liste vide)
	 */
	public static List<Category> getCategoryList(int userId) {
		List<Category> categories = new ArrayList<Category>();
		Connection connection = DBConnection.getConnection();
		
		try {
			PreparedStatement statement = connection.prepareStatement("SELECT id, id_user, nom_categorie FROM categories WHERE id_user = ? ");
			statement.setInt(1, userId);
			statement.close();
			ResultSet result = statement.executeQuery();
			int id, id_user;
			String nom_categorie = "";
			
			while(result.next()) {
			id = result.getInt(1);
			id_user = result.getInt(2);
			nom_categorie = result.getString(3);
			categories.add(new Category(id, id_user, nom_categorie));
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		
		return categories;
	}
}
