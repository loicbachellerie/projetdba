package ca.qc.cvm.dba.memos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ca.qc.cvm.dba.memos.entity.User;
import ca.qc.cvm.dba.memos.util.BCrypt;

public class UserDAO {

	/**
	 * En fonction d'un nom d'usager et mot de passe, v�rifier si l'usager existe bel et bien dans la BD
	 * 
	 * @param username nom d'usager
	 * @param password mot de passe
	 * @return l'usager ou null si les informations sont erron�es
	 */
	public static User login(String username, char[] password) {
		User user = null;
		Connection connection = DBConnection.getConnection();
		
		try {
			PreparedStatement statement = connection.prepareStatement("SELECT id, username, password FROM users WHERE username = ? ");
			statement.setString(1, username);
			
			ResultSet result = statement.executeQuery();
			int id = 0;
			String usr = "";
			String pass = "";
			
			while(result.next()) {
			id = result.getInt(1);
			usr = result.getString(2);
			pass = result.getString(3);
			}
			statement.close();
			
			String pwd = pass;

			if (BCrypt.checkpw(String.valueOf(password), pwd)) {
						 user = new User(id, pwd);
				}
			else {
						return null;
				}

			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		
		
		return user;
	}
	
	/**
	 * M�thode qui permet d'enregistrer un nouveau membre
	 * 
	 * @param username nom d'usager
	 * @param password son mot de passe
	 * @return
	 */
	
	public static boolean register(String username, char[] password) {
		String motDePasse = BCrypt.hashpw(String.valueOf(password), BCrypt.gensalt());
		boolean success = false;
		Connection connection = DBConnection.getConnection();
		
		
		try {
			PreparedStatement statement;
			statement = connection.prepareStatement("INSERT INTO users(username, password) VALUES(?, ?)");
			statement.setString(1, username);
			statement.setString(2, motDePasse);
			statement.execute();
			statement.close();
			success = true;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		// Doit retourner success � "true" lorsque l'enregistrement a fonctionn�
		
		return success;
	}
}
