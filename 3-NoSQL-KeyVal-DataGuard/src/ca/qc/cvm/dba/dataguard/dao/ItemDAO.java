package ca.qc.cvm.dba.dataguard.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import com.sleepycat.je.Cursor;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.LockMode;
import com.sleepycat.je.OperationStatus;

import ca.qc.cvm.dba.dataguard.entity.Item;

public class ItemDAO {

	/**
	 * M�thode permettant d'ajouter un item/fichier dans la base de donn�es
	 * 
	 * @param key (ou cl�)
	 * @param fileData (ou valeur)
	 * @return vrai/faux, selon si c'est un succ�s ou �chec
	 */
	public static boolean addItem(String key, byte[] fileData) {
		boolean success = false;
		Database connection = DBConnection.getConnection();
		 
		try {
		    DatabaseEntry theKey = new DatabaseEntry(key.getBytes("UTF-8"));
		    DatabaseEntry theData = new DatabaseEntry(fileData);
		    connection.put(null, theKey, theData); 
		    success = true;
		    return success;
		} 
		catch (Exception e) {
		    // Exception handling
			return false;
		}
 
	}

	/**
	 * M�thode utilis�e pour supprimer un item/fichier
	 * 
	 * @param key
	 * @return vrai/faux, selon si c'est un succ�s ou �chec 
	 */
	public static boolean deleteItem(String key) {
		boolean success = false;

        Database connection = DBConnection.getConnection();
        
        try {
            DatabaseEntry theKey = new DatabaseEntry(key.getBytes("UTF-8"));
         connection.delete(null, theKey);
         success = true;
         return success;
        } 
        catch (Exception e) {
            // Exception handling
        	return false;
        }

	}
	
	/**
	 * Permet d'avoir acc�s � la liste des items/fichiers de la base de donn�es
	 * 
	 * @return la liste des Item de la base de donn�es (leur cl�, pas leur valeur)
	 */
	public static List<Item> getItemList() {
		List<Item> items = new ArrayList<Item>();
		Database connection = DBConnection.getConnection();
		Cursor myCursor = null;

		try {
		    myCursor = connection.openCursor(null, null);
		 
		    DatabaseEntry foundKey = new DatabaseEntry();
		    DatabaseEntry foundData = new DatabaseEntry();
		 
		    while (myCursor.getNext(foundKey, foundData, LockMode.DEFAULT) == OperationStatus.SUCCESS) {
		        String keyString = new String(foundKey.getData(), "UTF-8");
		        items.add(new Item(keyString));
		        //String dataString = new String(foundData.getData(), "UTF-8");
		        //System.out.println("Cl�|Valeur: " + keyString + " | " + dataString + "");
		    }
		    return items;
		} 
		catch (DatabaseException de) {
		    System.err.println("Erreur de lecture de la base de donn�es: " + de);
		    return items;
		} 
		catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return items;
		} 
		finally {
		    try {
		        if (myCursor != null) {
		            myCursor.close();
		        }
		    } 
		 catch(DatabaseException dbe) {
		        System.err.println("Erreur de fermeture du curseur: " + dbe.toString());
		    }
		}
	}
	
	/**
	 * Puisque le programme poss�de des fichiers, cette m�thode permet de 
	 * r�cup�rer un �l�ment de la base de donn�es afin de recr�er le fichier � l'endroit voulu
	 * 
	 * @param key 
	 * @param destinationFile endroit de destination
	 * @return vrai/faux, selon si c'est un succ�s ou �chec 
	 */
	public static boolean restoreItem(String key, File destinationFile) {
		boolean success = false;
		Database connection = DBConnection.getConnection();

		try {
		    DatabaseEntry theKey = new DatabaseEntry(key.getBytes("UTF-8"));
		    DatabaseEntry theData = new DatabaseEntry();
		 
		    if (connection.get(null, theKey, theData, LockMode.DEFAULT) == OperationStatus.SUCCESS) { 
		        byte[] retData = theData.getData();
		        String foundData = new String(retData, "UTF-8");

		        System.out.println("Cl�: '" + key + "' donn�e: '" + foundData + "'.");
		        FileOutputStream fos = new FileOutputStream(destinationFile);
		        fos.write(retData);
		        fos.close();
		        success = true;
		        return success;
		    } 
		 else {
		        System.out.println("Element inexistant");
		        return false;
		   
		    }
		} 
		catch (Exception e) {
			return false;
		}
        
        
	}
}
