package ca.qc.cvm.dba.scoutlog.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

public class Neo4jConnection {
	private final static String DB_NAME = "ScoutDB";
	private static GraphDatabaseService graphDB;

	/**
	 * M�thode permettant d'obtenir une connexion
	 * @return une connexion
	 */
	public static GraphDatabaseService getConnection() {
		if (graphDB == null) {
			graphDB = new GraphDatabaseFactory().newEmbeddedDatabase(new File(DB_NAME));
			
		    Runtime.getRuntime().addShutdownHook( new Thread() {
		        @Override
		        public void run() {
		        	graphDB.shutdown();
		        }
		    } );
		}
		
		return graphDB;
	}
	
	/**
	 * M�thode permettant de parcourir un chemin et de retirer la liste de noeuds/relations de celui-ci
	 * 
	 * Exemple : MATCH chemin=(n)-[r:CONNAIT]->(n2) RETURN chemin
	 * Retournerait une liste du genre [Noeud1, Relation1a2, Noeud2, Relation2a3, Noeud3]
	 * 
	 * @param query La requ�te Cypher
	 * @param params Les param�tres de la requ�te
	 * @return Une liste de noeuds et relations.
	 */
	public static List<Object> getPath(String query, Map<String, Object> params) {
		GraphDatabaseService connection = Neo4jConnection.getConnection();
		List<Object> resultList = new ArrayList<Object>();

		Result result = connection.execute(query, params);
			
		while ( result.hasNext() ) {
			Map<String,Object> row = result.next();
				
			for ( Entry<String, Object> column : row.entrySet() ) {
				Path p = (Path)(column.getValue());
				Iterator<Node> nodes = p.nodes().iterator();
				Iterator<Relationship> relationships= p.relationships().iterator();
				
				while (nodes.hasNext()) {
					Node node = nodes.next();
		
					resultList.add(node);
					
					if (relationships.hasNext()) {
						resultList.add(relationships.next());
					}
				}						
			}
		}
				
		return resultList;
	}
}
