package ca.qc.cvm.dba.scoutlog.dao;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.mongodb.DBCursor;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import ca.qc.cvm.dba.scoutlog.entity.LogEntry;

public class LogDAO {
	/**
	 * M�thode permettant d'ajouter une entr�e
	 * 
	 * Note : Ne changer pas la structure de la m�thode! Elle
	 * permet de faire fonctionner l'ajout d'une entr�e du journal.
	 * Il faut donc que la compl�ter.
	 * 
	 * @param log
	 * @return
	 */
	
	static MongoDatabase connection = MongoConnection.getConnection();
	static MongoCollection<Document> collection = connection.getCollection("scout");
	
	public static boolean addLog(LogEntry log) {
		boolean success = false;
		
		String status = log.getStatus();
		Document doc = new Document();
		
		if(status.equals("Normal")){
			System.out.println("Status = Normal");
			
			doc.append("Commandant", log.getName());
			doc.append("Date", log.getDate());
			success =true;
			
		}
		else if(status.equals("Anormal")){
			System.out.println("Status = Anormal");
			
			doc.append("Commandant", log.getName());
			doc.append("Data", log.getDate());
			doc.append("Raisons", log.getReasons());
			success =true;
			
		}
		else if(status.equals("Exploration")){
			System.out.println("Status = Exploration");
			
			doc.append("Commandant", log.getName());
			doc.append("Data", log.getDate());
			doc.append("PlanetePres", log.getNearPlanets());
			doc.append("Planete", log.getPlanetName());
			doc.append("galaxie", log.getGalaxyName());
			doc.append("Habitable", log.isHabitable());
			success = true;
			
		}
		
		if (log.getImage() != null) {
			// l'entr�e poss�de une image!
			doc.append("image", log.getImage());
		}
		
		System.out.println(log.toString());
		collection.insertOne(doc);
		return success;
	}
	
	/**
	 * Permet de retourner la liste de plan�tes d�j� explor�es
	 * 
	 * Note : Ne changer pas la structure de la m�thode! Elle
	 * permet de faire fonctionner l'ajout d'une entr�e du journal.
	 * Il faut donc que la compl�ter.
	 * 
	 * @return le nom des plan�tes d�j� explor�es
	 */
	public static List<String> getPlanetList() {
		List<String> planets = new ArrayList<String>();
	
		
		// Exemple...
		planets.add("Terre");
		planets.add("Solaria");
		planets.add("Dune");
		
		return planets;
	}
	
	/**
	 * Retourne l'entr�e selon sa position dans le temps.
	 * La derni�re entr�e est 0,
	 * l'avant derni�re est 1,
	 * l'avant avant derni�re est 2, etc.
	 * 
	 * Toutes les informations li�es � l'entr�e doivent �tre affect�es � 
	 * l'objet retourn�. 
	 * 
	 * Note : Ne changer pas la structure de la m�thode! Elle
	 * permet de faire fonctionner l'affichage de la liste des entr�es 
	 * du journal. Il faut donc que la compl�ter.
	 * 
	 * @param position (d�marre � 0)
	 * @return
	 */
	public static LogEntry getLogEntryByPosition(int position) {
		return null;
	}
	
	/**
	 * Doit retourner le nombre d'entr�es dans le journal de bord
	 * 
	 * Note : Ne changer pas la structure de la m�thode! Elle
	 * permet de faire fonctionner l'affichage de la liste des entr�es 
	 * du journal. Il faut donc que la compl�ter.
	 * 
	 * @return nombre total
	 */
	public static int getNumberOfEntries() {
		return 0;
	}
	
	/**
	 * Retourne le nombre de plan�tes habitables
	 * 
	 * @return nombre total
	 */
	public static int getNumberOfHabitablePlanets() {
		return 0;
	}
	
	/**
	 * Suppression de toute les donn�es
	 */
	public static boolean deleteAll() {
		boolean success = false;
		
		return success;
	}
}
