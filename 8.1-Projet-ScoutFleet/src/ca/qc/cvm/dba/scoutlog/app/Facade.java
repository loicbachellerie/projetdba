package ca.qc.cvm.dba.scoutlog.app;

import java.util.List;
import java.util.Observer;

import ca.qc.cvm.dba.scoutlog.dao.LogDAO;
import ca.qc.cvm.dba.scoutlog.entity.LogEntry;
import ca.qc.cvm.dba.scoutlog.event.CommonEvent;

/**
 * Cette classe est l'interm�diaire entre la logique et la vue
 * Entre les panel et le MngApplication. C'est le point d'entr�e de la vue
 * vers la logique
 */
public class Facade {
	private static Facade instance;
	
	private MngApplication app;
	
	private Facade() {
		app = new MngApplication();
	}
	
	public static Facade getInstance() {
		if (instance == null) {
			instance = new Facade();
		}
		
		return instance;
	}
	
	public void processEvent(CommonEvent event) {
		app.addEvent(event);
        new Thread(app).start();
	}
	
	public void addObserverClass(Observer o) {
		app.addObserver(o);
	}
	
	public List<String> getPlanetList() {
		return app.getPlanetList();
	}
	
	public int getNumberOfEntries() {
		return app.getNumberOfEntries();
	}
	
	public LogEntry getLogEntryByPosition(int position) {
		return app.getLogEntryByPosition(position);
	}
		
	public int getNumberOfHabitablePlanets() {
		return app.getNumberOfHabitablePlanets();
	}
	
	public void exit() {
		app.exit();
	}
}
