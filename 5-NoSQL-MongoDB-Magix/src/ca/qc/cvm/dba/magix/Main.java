package ca.qc.cvm.dba.magix;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.UIManager;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

import ca.qc.cvm.dba.magix.app.Facade;
import ca.qc.cvm.dba.magix.dao.DBConnection;
import ca.qc.cvm.dba.magix.view.GameMagix;

public class Main {
	public static void main(String[] args) throws Exception {
        try {
 			AppGameContainer appgc;
 			appgc = new AppGameContainer(new GameMagix("Magix"));
 			appgc.setDisplayMode(950, 650, false);
 			appgc.setClearEachFrame(true);
 			appgc.setMinimumLogicUpdateInterval(30);
 		    appgc.setTargetFrameRate(60);
 		    appgc.setShowFPS(false);
 		    appgc.setIcon("assets/images/icon.png");
 		    
 		    Facade.getInstance().testConnection();
 			
 		    appgc.start();
 		}
 		catch (SlickException ex)
 		{
 			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
 		}
 	}
}