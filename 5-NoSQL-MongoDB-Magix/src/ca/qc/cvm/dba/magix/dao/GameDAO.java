package ca.qc.cvm.dba.magix.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.bson.Document;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.CommandResult;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import ca.qc.cvm.dba.magix.entity.Card;

public class GameDAO {
	/**
	 * M�thode retournant le nombre de parties jou�es
	 * @return nbre
	 */
	public static long getGameCount() {
		MongoDatabase connection = DBConnection.getConnection();
		
		return 0;
	}
	
	/**
	 * Permet d'obtenir des informations sur les derni�res parties jou�es
	 * 
	 * @param numberOfResults Nombre de partie � retourner
	 * @return Une liste contenant les informations des derni�res parties jou�es
	 */
	public static List<String> getLatestGamesResults(int numberOfResults) {
		final List<String> results = new ArrayList<String>();
		MongoDatabase connection = DBConnection.getConnection();
				
		return results;
	}
	
	/**
	 * Permet de savoir, pour le joueur, le nombre de parties 
	 * qui ont �t� gagn�es avec cette carte dans leur s�lection.
	 * 
	 * Cette m�thode ne doit pas v�rifier pour l'IA, seulement le vrai joueur
	 * 
	 * @param c Carte � v�rifier
	 * @return Le nombre de parties
	 */
	private static long getNumberOfWonGames(Card c) {
		MongoDatabase connection = DBConnection.getConnection();
		
		return 0;
	}
	
	/**
	 * Nombre de tours moyen avant que la partie se compl�te. 
	 * 
	 * @return
	 */
	public static double getAverageRounds() {
		MongoDatabase connection = DBConnection.getConnection();
		
		return 0;
	}
	
	/**
	 * Cette m�thode est appel�e lorsqu'une partie se termine.
	 * 
	 * @param playerCards Les cartes choisies par l'usager
	 * @param aiCards Les cartes choisies par l'IA (adversaire)
	 * @param playerLife La vie restante du joueur � la fin de la partie
	 * @param aiLife La vie restante de l'IA � la fin de la partie
	 * @param totalRounds Le nombre de tours jou�s avant la fin de la partie
	 */
	public static void logGame(List<Card> playerCards, List<Card> aiCards, int playerLife, int aiLife, int totalRounds) {
		
	}
	
	/**
	 * Permet de savoir pour chaque carte combien de fois elle furent
	 * pr�sente lorsque l'usager a gagn�.
	 * 
	 * @param collection de cartes du jeu
	 * @return Une liste d'objets, o� chaque object est similaire � : {"Wolf", 3}, 
	 * ce qui signifie que la carte "Wolf" a �t� pr�sente 3 fois dans les parties gagn�es du joueur 
	 */
	public static List<Object[]> getCardRankings(List<Card> collection) {
		List<Object[]> rankings = new ArrayList<Object[]>();
		
		for (Card c : collection) {
			rankings.add(new Object[]{c.getName(), getNumberOfWonGames(c)});
		}
		
		return sortCardsByRanking(rankings);
	}	
	
	/**
	 * M�thode permettant de trier les objets en ordre d�croissant
	 * 
	 * Exemple de liste valide:
	 * rankings.get(0) contient : Object[]{"Wolf", 4}
	 * rankings.get(1) contient : Object[]{"Dragon", 2}
	 * rankings.get(2) contient : Object[]{"Swift", 5}
	 * 
	 * Ceci retournera
	 * rankings.get(0) contient : Object[]{"Swift", 5}
	 * rankings.get(1) contient : Object[]{"Wolf", 4}
	 * rankings.get(2) contient : Object[]{"Dragon", 2}
	 * 
	 * @param rankings � trier
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static List<Object[]> sortCardsByRanking(List<Object[]> rankings) {
		Collections.sort(rankings, new Comparator() {

			@Override
			public int compare(Object o1, Object o2) {
				int result = 0;
				
				if ((long)(((Object[])o1)[1]) < (long)(((Object[])o2)[1])) {
					result = 1;
				}
				else if ((long)(((Object[])o1)[1]) > (long)(((Object[])o2)[1])) {
					result = -1;
				}
				return result;
			}
			
		});
		
		return rankings;
	}
}
