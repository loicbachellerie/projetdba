package ca.qc.cvm.dba.magix.entity;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;

public abstract class Card {
	public enum CardType {Minion, Spell};
	
	private String name;
	private Image image;
	private CardType type;
	private int cost;

	private static Image frame;
	private static Image coin;
	private static Color disabledOverlay = new Color(0f,0f,0f,0.75f);
	private static TrueTypeFont descriptionFont;
	
	public Card(String name, CardType type, int cost) {
		this.name = name;
		this.type = type;
		this.cost = cost;
		
		if (frame == null) {
			try {
				frame = new Image("assets/images/frame-card-sm.png");
				coin = new Image("assets/images/coin.png");
				
				java.awt.Font font = new java.awt.Font("Verdana", java.awt.Font.BOLD, 10);
				descriptionFont = new TrueTypeFont(font, true);
			}
			catch (SlickException e) {}
		}

		try {
			image = new Image("assets/images/" + name.toLowerCase() + "-sm.png");
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
	
	public CardType getType() {
		return type;
	}
	
	public String getName() {
		return name;
	}
	
	public int getCost() {
		return cost;
	}

	public void render(Graphics g, int x, int y, boolean active) {

		g.drawImage(image, x, y);
		g.drawImage(frame, x, y);
		
		if (type == CardType.Minion) {
			g.setColor(Color.white);
		}
		else {
			g.setColor(Color.magenta);
		}
		
		g.drawString(name, x + 8, y + 135);
		g.setColor(Color.gray);
		
		if (type == CardType.Minion) {
			g.drawString("Minion", x + 45, y + 200);
		}
		else {
			g.drawString("Spell", x + 50, y + 200);
		}
		
		g.drawImage(coin, x + 110, y + 120);
		g.setColor(Color.black);
		g.drawString("" + cost, x + 121, y + 127);
		
		Font font = g.getFont();
		g.setFont(descriptionFont);
		g.setColor(Color.white);
		renderCard(g, x + 8, y + 160);
		g.setFont(font);
		
		if (!active) {
			Color originalColor = g.getColor();
			
			g.setColor(disabledOverlay);
			g.fillRect(x, y, frame.getWidth(), frame.getHeight()); //draw onto the alpha map
			
			g.setDrawMode(Graphics.MODE_NORMAL);
			g.setColor(originalColor);
		}
	}
	
	protected abstract void renderCard(Graphics g, int x, int y); 
}
