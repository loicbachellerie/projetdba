package ca.qc.cvm.dba.magix.view;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import ca.qc.cvm.dba.magix.app.Facade;
import ca.qc.cvm.dba.magix.event.GoToEvent;
import ca.qc.cvm.dba.magix.sprite.Background;
import ca.qc.cvm.dba.magix.sprite.Button;
import ca.qc.cvm.dba.magix.sprite.Label;
import ca.qc.cvm.dba.magix.sprite.Text;
import ca.qc.cvm.dba.magix.util.ActionHandler;

public class SceneAbout extends Scene{

	public SceneAbout() {
		
	}
	
	public void init() throws SlickException {
		
		Image startImage = new Image("assets/images/button.png");
		Image startImageOver = new Image("assets/images/button-over.png");
		Button startButton = new Button(startImage, startImageOver, "Retour", new ActionHandler() {
 		    public void onAction() {
 		    	Facade.getInstance().processEvent(new GoToEvent(GoToEvent.Destination.Splash));
 		    }
 		});
		startButton.setCenterPosition(800, 600);
		super.registerSprite(startButton);
		
		Text text = null;
		
		try {
			text = new Text(10, 10, new String(Files.readAllBytes(new File("assets/text/about.txt").toPath())));
		} 
		catch (IOException e) {
			text = new Text(10, 10, "Cannot read file");
		}
		
		super.registerSprite(text);
	}
	
	@Override
	public void updateScene(int mouseX, int mouseY, boolean mousePressed) {
		
	}
	
	@Override
	protected void renderScene(Graphics g) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
