package ca.qc.cvm.dba.magix.view;

import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import ca.qc.cvm.dba.magix.app.Facade;
import ca.qc.cvm.dba.magix.entity.Card;
import ca.qc.cvm.dba.magix.event.ChooseCardEvent;
import ca.qc.cvm.dba.magix.event.GoToEvent;
import ca.qc.cvm.dba.magix.sprite.Background;
import ca.qc.cvm.dba.magix.sprite.Button;
import ca.qc.cvm.dba.magix.sprite.Label;
import ca.qc.cvm.dba.magix.sprite.Text;
import ca.qc.cvm.dba.magix.util.ActionHandler;

public class SceneChoose extends Scene {
	private List<Card> cardList;
	private int maxAllowedCopies;
	
	private Button coin;
	private Button playButton;
	private Text textNum;
	private Text cardNames;

	public SceneChoose() {
		
	}
	
	public void init() throws SlickException {
		Background background = new Background(new Image("assets/images/background-fort.png"));
		super.registerSprite(background);
		
		Image playImage = new Image("assets/images/button.png");
		Image playImageOver = new Image("assets/images/button-over.png");
		playButton = new Button(playImage, playImageOver, "Jouer!", new ActionHandler() {
 		    public void onAction() {
 		    	Facade.getInstance().processEvent(new GoToEvent(GoToEvent.Destination.Game));
 		    }
 		});
		playButton.setCenterPosition(800, 600);
		super.registerSprite(playButton);
		
		Image coinImage = new Image("assets/images/coin.png");
		coin = new Button(coinImage, coinImage, "20", Color.black, null);
		coin.setCenterPosition(50, 560);
		super.registerSprite(coin);
		
		Text text = new Text(80, 550, "jetons restants");
		super.registerSprite(text);
		
		textNum = new Text(45, 590, "", Color.orange);
		super.registerSprite(textNum);
		
		Text text2 = new Text(80, 590, "carte(s) dans votre deck");
		super.registerSprite(text2);
		
		cardNames = new Text(45, 500, "", Color.orange);
		super.registerSprite(cardNames);
	}
	
	@Override
	public void updateScene(int mouseX, int mouseY, boolean mousePressed) {
	}
	
	@Override
	public void mouseClicked(int mouseX, int mouseY) {
		super.mouseClicked(mouseX, mouseY);
		int x = 10;
		int y = 10;
		
		for (Card card : cardList) {
			if (mouseX > x && mouseX < x + 150 &&
				mouseY > y && mouseY < y + 225) {
				
				if (Facade.getInstance().getPlayerRemainingCoins() >= card.getCost() && countCopies(card) < maxAllowedCopies) {
					Facade.getInstance().processEvent(new ChooseCardEvent(card));
				}
			}
			
			x += 155;
			
			if (x > 800) {
				x = 10;
				y += 235;
			}
		}
	}
	
	public int countCopies(Card card) {
		int copies = 0;
		
		for (Card c : Facade.getInstance().getPlayerCardList()) {
			if (c.getName().equals(card.getName())) {
				copies++;
			}
		}
		
		return copies;
	}
	
	@Override
	protected void renderScene(Graphics g) {
		int x = 10;
		int y = 10;
		int remainingCoins = Facade.getInstance().getPlayerRemainingCoins();
		
		for (Card card : cardList) {
			card.render(g, x, y, remainingCoins >= card.getCost() && countCopies(card) < maxAllowedCopies);
			x += 155;
			
			if (x > 800) {
				x = 10;
				y += 235;
			}
		}
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
		cardList = Facade.getInstance().getCardCollection();
		coin.setText(Facade.getInstance().getPlayerRemainingCoins() + "");
		playButton.setEnabled(Facade.getInstance().getPlayerRemainingCoins() == 0);
		textNum.setText(Facade.getInstance().getPlayerCardList().size() + "");
		maxAllowedCopies = Facade.getInstance().getMaximumAllowedCopies();
		
		String text = "";
		
		for (Card card : Facade.getInstance().getPlayerCardList()) {
			text += card.getName() + ", "; 
		}
		
		if (text.length() > 0) {
			text = text.substring(0, text.length() - 2);
		}
		
		cardNames.setText(text);
	}
}
