package ca.qc.cvm.dba.magix.view;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import ca.qc.cvm.dba.magix.sprite.Sprite;

public abstract class Scene {
	public List<Sprite> spriteList;
	
	public Scene() {
		spriteList = new ArrayList<Sprite>();
	}
	
	protected void registerSprite(Sprite sprite) {
		this.spriteList.add(sprite);
	}
		
	public void update(int mouseX, int mouseY, boolean mousePressed) {
		for (Sprite sprite : spriteList) {
			sprite.update(mouseX, mouseY, mousePressed);
	    }
		
		updateScene(mouseX, mouseY, mousePressed);
	}
	
	public void mouseClicked(int mouseX, int mouseY) {
		for (Sprite sprite : spriteList) {
	    	
	        if (sprite.getActionHandler() != null && sprite.checkInside(mouseX, mouseY) && sprite.isEnabled()) {
	        	sprite.getActionHandler().onAction();
	        }
	    }		
	}
	
	public void render(Graphics g) {
		for (Sprite sprite : spriteList) {
			sprite.render(g);
		}
		
		renderScene(g);
	}
	
	public void refresh() {
		resume();
	}
	
	protected abstract void updateScene(int mouseX, int mouseY, boolean mousePressed);
	protected abstract void renderScene(Graphics g);
	public abstract void init() throws SlickException;
	public abstract void pause();
	public abstract void resume();
}
