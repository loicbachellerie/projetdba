package ca.qc.cvm.dba.magix.view;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import ca.qc.cvm.dba.magix.app.Facade;
import ca.qc.cvm.dba.magix.event.GoToEvent;
import ca.qc.cvm.dba.magix.sprite.Background;
import ca.qc.cvm.dba.magix.sprite.Button;
import ca.qc.cvm.dba.magix.sprite.Label;
import ca.qc.cvm.dba.magix.sprite.Text;
import ca.qc.cvm.dba.magix.util.ActionHandler;

public class SceneInfo extends Scene {
	private Text text;

	public SceneInfo() {
		
	}
	
	public void init() throws SlickException {
		
		Image startImage = new Image("assets/images/button.png");
		Image startImageOver = new Image("assets/images/button-over.png");
		Button startButton = new Button(startImage, startImageOver, "Retour", new ActionHandler() {
 		    public void onAction() {
 		    	Facade.getInstance().processEvent(new GoToEvent(GoToEvent.Destination.Splash));
 		    }
 		});
		startButton.setCenterPosition(800, 600);
		super.registerSprite(startButton);
		
		text = new Text(10, 10, "");
		super.registerSprite(text);
	}
	
	@Override
	public void updateScene(int mouseX, int mouseY, boolean mousePressed) {
		
	}
	
	@Override
	protected void renderScene(Graphics g) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
		long current = System.currentTimeMillis();
		String ranking = "";
		
		List<Object[]> rankings = Facade.getInstance().getCardRankings();
		
		for (int i = 0; i < rankings.size(); i++) {
			ranking += rankings.get(i)[0] + "(" + rankings.get(i)[1] + ") ";
		
			if (i == 5) {
				ranking += "\n";
			}
		}
		
		String txt = Facade.getInstance().getGameCount() + " parties ont �t� jou�es au total.\n";
		txt += "Nombre moyen de tours par partie: " + Facade.getInstance().getAverageRounds() + "\n\n";
		txt += "Classement des cartes:\n" + ranking + "\n\n";
		txt += "Les derniers r�sultats:\n";
		
		for (String result : Facade.getInstance().getLatestResults(15)) {
			txt += "- " + result + "\n";
		}	
		
		double time = (System.currentTimeMillis() - current)/1000.0;
		
		txt += "\n\nPage g�n�r�e en : " + time + " sec.";
		
		text.setText(txt);
	}
}
