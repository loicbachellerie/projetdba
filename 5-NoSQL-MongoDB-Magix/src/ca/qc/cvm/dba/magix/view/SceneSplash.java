package ca.qc.cvm.dba.magix.view;

import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import ca.qc.cvm.dba.correctionserver.lib.CorrectionDialog;
import ca.qc.cvm.dba.magix.app.Facade;
import ca.qc.cvm.dba.magix.event.BatchEvent;
import ca.qc.cvm.dba.magix.event.CorrectionEvent;
import ca.qc.cvm.dba.magix.event.ExitEvent;
import ca.qc.cvm.dba.magix.event.GoToEvent;
import ca.qc.cvm.dba.magix.sprite.Background;
import ca.qc.cvm.dba.magix.sprite.Button;
import ca.qc.cvm.dba.magix.sprite.Label;
import ca.qc.cvm.dba.magix.util.ActionHandler;

public class SceneSplash extends Scene{

	public SceneSplash() {
		
	}
	
	public void init() throws SlickException {
		Background background = new Background(new Image("assets/images/background-landscape.png"));
		super.registerSprite(background);
		
		Label label = new Label(new Image("assets/images/title.png"));
		label.setCenterPosition(475, 200);
		super.registerSprite(label);
		
		Image startImage = new Image("assets/images/button.png");
		Image startImageOver = new Image("assets/images/button-over.png");
		Button startButton = new Button(startImage, startImageOver, "D�marrer", new ActionHandler() {
 		    public void onAction() {
 		    	Facade.getInstance().processEvent(new GoToEvent(GoToEvent.Destination.Choose));
 		    }
 		});
		startButton.setCenterPosition(475, 320);
		super.registerSprite(startButton);
		
		Image aboutImage = new Image("assets/images/button.png");
		Image aboutImageOver = new Image("assets/images/button-over.png");
		Button aboutButton = new Button(aboutImage, aboutImageOver, "� propos de", new ActionHandler() {
 		    public void onAction() {
 		        Facade.getInstance().processEvent(new GoToEvent(GoToEvent.Destination.About));
 		    }
 		});
		aboutButton.setCenterPosition(475, 390);
		super.registerSprite(aboutButton);

		Image exitImage = new Image("assets/images/button.png");
		Image exitImageOver = new Image("assets/images/button-over.png");
		Button exitButton = new Button(exitImage, exitImageOver, "Quitter", new ActionHandler() {
 		    public void onAction() {
 		    	Facade.getInstance().processEvent(new ExitEvent());
 		    }
 		});
		exitButton.setCenterPosition(475, 460);
		super.registerSprite(exitButton);
		
		Image batchImage = new Image("assets/images/button-sm.png");
		Image batchImageOver = new Image("assets/images/button-over-sm.png");
		Button batchButton = new Button(batchImage, batchImageOver, "Batch", new ActionHandler() {
 		    public void onAction() {
 		    	String number = JOptionPane.showInputDialog("Nombre de parties?", "10");
 		    	
 		    	if (number != null) {
	 		    	try {
	 		    		int num = Integer.parseInt(number);
	 		    		Facade.getInstance().processEvent(new BatchEvent(num));
	 		    	}
	 		    	catch (Exception e){}
 		    	}
 		    }
 		});
		batchButton.setCenterPosition(60, 620);
		super.registerSprite(batchButton);
		
		Image infoImage = new Image("assets/images/button-sm.png");
		Image infoImageOver = new Image("assets/images/button-over-sm.png");
		Button infoButton = new Button(infoImage, infoImageOver, "Info", new ActionHandler() {
 		    public void onAction() {
 		    	Facade.getInstance().processEvent(new GoToEvent(GoToEvent.Destination.Info));
 		    }
 		});
		infoButton.setCenterPosition(890, 620);
		super.registerSprite(infoButton);

		
		Image corImage = new Image("assets/images/button-sm.png");
		Image corImageOver = new Image("assets/images/button-over-sm.png");
		Button corButton = new Button(corImage, corImageOver, "Correct", new ActionHandler() {
 		    public void onAction() {
 		    	String[] data = CorrectionDialog.getData(null);
				
				if (data != null) {
					Facade.getInstance().processEvent(new CorrectionEvent(data[0], data[1], data[2]));
				}
 		    }
 		});
		corButton.setCenterPosition(60, 30);
		super.registerSprite(corButton);
		
	}
	
	@Override
	public void updateScene(int mouseX, int mouseY, boolean mousePressed) {
		
	}
	
	@Override
	protected void renderScene(Graphics g) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
