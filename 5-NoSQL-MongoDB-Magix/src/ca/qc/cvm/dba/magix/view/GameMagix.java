package ca.qc.cvm.dba.magix.view;

import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JOptionPane;

import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import ca.qc.cvm.dba.magix.app.Facade;
import ca.qc.cvm.dba.magix.event.ExitEvent;
import ca.qc.cvm.dba.magix.event.UIEvent;

public class GameMagix extends BasicGame implements Observer{
	private Scene currentScene;
	
	private int mouseX;
	private int mouseY;
	private boolean mouseClicked = false;
	private boolean mousePressed = false;

	public enum State {Splash, About, Choose, Game, Info}
	
	private HashMap<State, Scene> scenes;
	
	public GameMagix(String gamename) {
		super(gamename);
		Facade.getInstance().addObserverClass(this);
	}
	
	@Override
	public void init(GameContainer gc) throws SlickException {
		scenes = new HashMap<State, Scene>();		
		scenes.put(State.Splash, new SceneSplash());
		scenes.put(State.About, new SceneAbout());
		scenes.put(State.Choose, new SceneChoose());
		scenes.put(State.Game, new SceneGame());
		scenes.put(State.Info, new SceneInfo());
		    
		for (Scene scene : scenes.values()) {
			scene.init();
		}
		
		this.setScene(State.Splash);
		gc.setMouseCursor("assets/images/cursor.png", 0, 0);
	}    
	
	@Override
    public boolean closeRequested() {
		Facade.getInstance().processEvent(new ExitEvent());
      
		return false;
    }
		
	public void setScene(State state) {
		if (currentScene != null) {
			currentScene.pause();
		}
		
		currentScene = scenes.get(state);
		currentScene.resume();
	}
	
	@Override
	public void update(GameContainer gc, int i) throws SlickException {
	      Input input = gc.getInput();
	      
	      if(input.isKeyPressed(Input.KEY_ESCAPE)) {
	         gc.exit();
	      }
	      
	      currentScene.update(mouseX, mouseY, mousePressed);
	      
	      if (mouseClicked) {
	    	  currentScene.mouseClicked(mouseX, mouseY);
	      }
	      
	      mouseClicked = false;
	}

	@Override
	public void update(Observable o, Object arg) {
		UIEvent event = ((UIEvent)arg);
		
		if (event.getUIType() == UIEvent.UIType.GoTo) {
			this.setScene(State.valueOf(event.getData().toString()));
		}
		else if (event.getUIType() == UIEvent.UIType.Refresh) {
			currentScene.refresh();
		}
		else if (event.getUIType() == UIEvent.UIType.ShowMessage) {
			JOptionPane.showMessageDialog(null, event.getData() + "");
		}
	}
	
	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		currentScene.render(g);		
	}

	@Override
	public void mouseClicked(int button, int x, int y, int clickCount) {  
	  mouseX = x;
	  mouseY = y;
	  mouseClicked = true;
	}
	
	@Override
	public void mousePressed(int button, int x, int y) {
		mouseX = x;
		mouseY = y;
		mousePressed = true;
	}
	
	@Override
	public void mouseReleased(int button, int x, int y) {
		mouseX = x;
		mouseY = y;
		mousePressed = false;
	}
		   
	@Override
	public void mouseDragged(int oldx, int oldy, int x, int y) {
		mouseX = x;
		mouseY = y;
	}
		   
	@Override
	public void mouseMoved(int oldx, int oldy, int x, int y) {
		mouseX = x;
		mouseY = y;
	}
}