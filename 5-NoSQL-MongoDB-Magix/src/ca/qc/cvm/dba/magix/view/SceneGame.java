package ca.qc.cvm.dba.magix.view;

import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;

import ca.qc.cvm.dba.magix.app.Facade;
import ca.qc.cvm.dba.magix.entity.Card;
import ca.qc.cvm.dba.magix.event.GoToEvent;
import ca.qc.cvm.dba.magix.sprite.Background;
import ca.qc.cvm.dba.magix.sprite.Button;
import ca.qc.cvm.dba.magix.sprite.Label;
import ca.qc.cvm.dba.magix.sprite.Text;
import ca.qc.cvm.dba.magix.util.ActionHandler;

public class SceneGame extends Scene {
	private Button playerLife;
	private Button aiLife;
	private TrueTypeFont resultFont;
	private Text resultText;
	private Button startOverButton;

	private List<Card> playerCardList;
	private List<Card> aiCardList;
	
	public SceneGame() {
		
	}
	
	public void init() throws SlickException {
		Background background = new Background(new Image("assets/images/background-forest.jpg"));
		super.registerSprite(background);

		Text text = new Text(40, 520, "VOUS");
		super.registerSprite(text);
		
		Image heartImage = new Image("assets/images/heart.png");
		playerLife = new Button(heartImage, heartImage, "--", null);
		playerLife.setCenterPosition(60, 590);
		super.registerSprite(playerLife);
		
		text = new Text(20, 110, "ADVERSAIRE");
		super.registerSprite(text);
		
		aiLife = new Button(heartImage, heartImage, "--", null);
		aiLife.setCenterPosition(60, 60);
		super.registerSprite(aiLife);

		java.awt.Font font = new java.awt.Font("Verdana", java.awt.Font.BOLD, 30);
		resultFont = new TrueTypeFont(font, true);
		
		resultText = new Text(40, 40, "");

		Image startImage = new Image("assets/images/button.png");
		Image startImageOver = new Image("assets/images/button-over.png");
		startOverButton = new Button(startImage, startImageOver, "Rejouer", new ActionHandler() {

			@Override
			public void onAction() {
				Facade.getInstance().processEvent(new GoToEvent(GoToEvent.Destination.Choose));
			}
		});
		
		startOverButton.setCenterPosition(475, 350);
		startOverButton.setVisible(false);
		super.registerSprite(startOverButton);
	}
	
	@Override
	public void updateScene(int mouseX, int mouseY, boolean mousePressed) {
		if (!Facade.getInstance().isGameFinished()) {
			Facade.getInstance().updateGame();
		}
	}
	
	private void drawCards(Graphics g, List<Card> cards, int y) {
		int round = Facade.getInstance().getRound();
		int x = 150;
		
		for (int i = (cards.size() > 8 && round > 8 ? round - 8 : 0 ); i < round && i < cards.size(); i++) {
			cards.get(i).render(g, x, y, true);
			x += 90;
		}
	}
	
	@Override
	protected void renderScene(Graphics g) {
		drawCards(g, aiCardList, 10);
		drawCards(g, playerCardList, 410);

		if (Facade.getInstance().isGameFinished()) {
			Font font = g.getFont();
			g.setFont(resultFont);
			
			resultText.setCenterPosition(g, 475, 270);
			resultText.render(g);
			
			g.setFont(font);
		}
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
		int playerLifeTmp = Facade.getInstance().getPlayerLife();
		int aiLifeTmp = Facade.getInstance().getAILife();
		
		playerLife.setText(playerLifeTmp + "");
		aiLife.setText(aiLifeTmp + "");
		
		playerCardList = Facade.getInstance().getPlayerCardList();
		aiCardList = Facade.getInstance().getAICardList();
		
		startOverButton.setVisible(Facade.getInstance().isGameFinished());
		
		if (Facade.getInstance().isGameFinished()) {
			if (aiLifeTmp < playerLifeTmp) {
				resultText.setColor(Color.magenta);
				resultText.setText("Vous avez gagn�!");
			}
			else if (aiLifeTmp > playerLifeTmp) {
				resultText.setColor(Color.red);
				resultText.setText("Vous avez perdu...");
			}
			else {
				resultText.setColor(Color.white);
				resultText.setText("La partie est nulle");
			}
		}
	}
}
