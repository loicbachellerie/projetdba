package ca.qc.cvm.dba.magix.sprite;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;

import ca.qc.cvm.dba.magix.util.ActionHandler;

public class Button extends Sprite {
	private Image image;
	private Image imageOver;
	private String text;
	private Color color;
	
	private static Color disabledOverlay = new Color(0, 0, 0, 0.5f);
	
	private boolean pressedOverButton = false;
	
	public Button(Image image, Image imageOver, String text, ActionHandler actionHandler) {
		this(image, imageOver, text, Color.white, actionHandler);
	}
	
	public Button(Image image, Image imageOver, String text, Color color, ActionHandler actionHandler) {
		super(new Rectangle(0, 0, image.getWidth(), image.getHeight()), actionHandler, true);
        this.image = image;
        this.imageOver = imageOver;
        this.text = text;
        this.color = color;
    }
	
	@Override
	public void update(int mouseX, int mouseY, boolean mousePressed) {
		if (actionHandler != null && super.isEnabled()) {
			if (super.checkInside(mouseX, mouseY) && mousePressed) {
				pressedOverButton = true;
			}
			else {
				pressedOverButton = false;
			}
		}
	}
	
	public void setText(String txt) {
		this.text = txt;
	}
	
	@Override
    public void renderSprite(Graphics g) {
		if (pressedOverButton) {
			g.drawImage(imageOver, shape.getX(), shape.getY());
		}
		else {
			g.drawImage(image, shape.getX(), shape.getY());
		}
		
		g.setColor(color);
		g.drawString(text, shape.getCenterX() - g.getFont().getWidth(text)/2, shape.getCenterY() - g.getFont().getHeight(text)/1.5f);
		
		if (!super.isEnabled()) { 
			Color originalColor = g.getColor();
			
			g.setColor(disabledOverlay);
			g.fillRect(shape.getX(), shape.getY(), shape.getWidth(), shape.getHeight());
			
			g.setDrawMode(Graphics.MODE_NORMAL);
			g.setColor(originalColor);
		}
    }
}
