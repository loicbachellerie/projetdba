package ca.qc.cvm.dba.magix.sprite;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;

public class Background extends Sprite {
	private Image image;

	public Background(Image image) {
		super(new Rectangle(0, 0, 950, 650));
		this.image = image;
	}

	@Override
	public void renderSprite(Graphics g) {
		g.drawImage(image, 0, 0);
	}

	@Override
	public void update(int mouseX, int mouseY, boolean mousePressed) {
	}
}
