package ca.qc.cvm.dba.magix.sprite;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

import ca.qc.cvm.dba.magix.util.ActionHandler;

public abstract class Sprite {
	private boolean enabled;
	protected Shape shape;
	private boolean visible;
	protected ActionHandler actionHandler;
	
	public Sprite(Shape shape) {
		this.shape = shape;
		this.visible = true;
	}

	public Sprite(Shape shape, ActionHandler actionHandler, boolean enabled) {
		this.shape = shape;
        this.actionHandler = actionHandler;
        this.enabled = enabled;
        this.visible = true;
	}
	
	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isEnabled() {
		return enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public ActionHandler getActionHandler() {
		return actionHandler;
	}
	
	public void setCenterPosition(int x, int y) {
		this.shape.setCenterX(x);
		this.shape.setCenterY(y);
	}
    
    public boolean checkInside(int x, int y) {
    	return shape.contains(x, y);
    }
    
    public final void render(Graphics g) {
    	if (visible) {
    		renderSprite(g);
    	}
    }
    
	public abstract void update(int mouseX, int mouseY, boolean mousePressed);
	protected abstract void renderSprite(Graphics g);

    
}
