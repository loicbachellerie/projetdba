package ca.qc.cvm.dba.magix.sprite;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;

public class Label extends Sprite {
	private Image image;

	public Label(Image image) {
		super(new Rectangle(0, 0, image.getWidth(), image.getHeight()));
		this.image = image;
	}

	@Override
	public void renderSprite(Graphics g) {
		g.setColor(Color.white);
		g.drawImage(image, this.shape.getX(), this.shape.getY());
	}

	@Override
	public void update(int mouseX, int mouseY, boolean mousePressed) {
	}
}
