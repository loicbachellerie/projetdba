package ca.qc.cvm.dba.magix.sprite;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

public class Text extends Sprite{
	private String text;
	private int x;
	private int y;
	private Color color;
	
	public Text(int x, int y, String text) {
		this(x, y, text, Color.white);
	}
	
	public Text(int x, int y, String text, Color color) {
		super(new Rectangle(0, 0, 0, 0));
        this.text = text;
        this.x = x;
        this.y = y;
        this.color = color;
    }
	
	@Override
	public void update(int mouseX, int mouseY, boolean mousePressed) {
		
	}

	public void setCenterPosition(Graphics g, int x, int y) {
		this.x = x - g.getFont().getWidth(text)/2;
		this.y = y - g.getFont().getHeight(text)/2;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	@Override
    public void renderSprite(Graphics g) {		
		g.setColor(color);
		g.drawString(text, x, y);
    }
}
